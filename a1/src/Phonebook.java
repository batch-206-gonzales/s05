import java.util.ArrayList;

public class Phonebook extends Contact{
    private ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook() {
        super();
    }

    public Phonebook(String name, String contactNumber, String address, ArrayList<Contact> contacts) {
        super(name, contactNumber, address);
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

    public void displayPhonebook() {
        if(this.contacts.isEmpty()) {
            System.out.println("The phonebook is currently empty.");
        } else {
            contacts.forEach(contact -> {
                System.out.println("---------------------");
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("my home in " + contact.getAddress());
            });
        }
    }
}
