public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        Contact contact2 = new Contact("John Doe", "+639162148573", "Caloocan City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        phonebook.displayPhonebook();
    }
}