public class StaticPoly {
    public int addition(int a, int b) {
        return a+b;
    }

    //Overloading by changing the number of arguments
    public int addition(int a, int b, int c) {
        return a+b+c;
    }

    //Overloading by changing the type of the arguments
    public double addition(double a, double b) {
        return a+b;
    }
}
