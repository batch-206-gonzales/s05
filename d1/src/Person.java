public class Person implements Action, Greetings{
    public void sleep(){
        System.out.println("Zzzzz....");
    }

    public void run(){
        System.out.println("Running on the road!");
    }

    public void morningGreet() {
        System.out.println("Good Morning, Friend!");
    }

    public void holidayGreet() {
        System.out.println("Happy holidays, Friend!");
    }
}
